﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestApplication.ReadifyService;

namespace TestApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var service = new RedPillClient("BasicHttpBinding_IRedPill");
            Console.WriteLine(service.WhatIsYourToken());
            Console.WriteLine(service.FibonacciNumber(10));
            Console.WriteLine(service.WhatShapeIsThis(10,10,10));
            Console.WriteLine(service.ReverseWords("Nitin"));
            Console.ReadLine();
        }
    }
}
