﻿using System;
using System.ServiceModel;
using KnockKnockService.Services;
using TriangleType = KnockKnockService.Models.TriangleType;

namespace KnockKnockService
{
    public class KnockKnock : IRedPill
    {

        private FibonacciService _fibonacciService;
        private TriangleService _triangleService;
        private ReverseWordsService _reverseWordsService;

        public FibonacciService FibonacciService
        {
            get
            {
                if (_fibonacciService == null)
                {
                    _fibonacciService = new FibonacciService();
                }
                return _fibonacciService;
            }
        }

        public TriangleService TriangleService
        {
            get
            {
                if (_triangleService == null)
                {
                    _triangleService = new TriangleService();
                }
                return _triangleService;
            }
        }

        public ReverseWordsService ReverseWordsService
        {
            get
            {
                if (_reverseWordsService == null)
                {
                    _reverseWordsService = new ReverseWordsService();
                }
                return _reverseWordsService;
            }
        }

        Guid IRedPill.WhatIsYourToken()
        {
            return new Guid("4a264a73-027c-45a9-b408-648875eb1b5d");
        }

        public long FibonacciNumber(long n)
        {
            try
            {
                return FibonacciService.GetNum(n);
            }
            catch (ArgumentOutOfRangeException e)
            {
                throw new FaultException<ArgumentOutOfRangeException>(e, e.Message);
            }
        }

        public TriangleType WhatShapeIsThis(int a, int b, int c)
        {
            try
            {
                return TriangleService.GetType(a, b, c);
            }
            catch (Exception e)
            {

                throw e;
            }

        }

        public string ReverseWords(string s)
        {
            try
            {
                return ReverseWordsService.ReverseWords(s);
            }
            catch (ArgumentNullException e)
            {
                throw new FaultException<ArgumentNullException>(e, e.Message);
            }
        }
    }
}
