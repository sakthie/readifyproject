﻿using System;
using System.Linq;


namespace KnockKnockService.Services
{
    public class ReverseWordsService
    {

        public string ReverseWords(string sentence)
        {
            if (sentence == null)
            {
                throw new ArgumentNullException();
            }

            var words = sentence.Split(' ').Select(ReverseWord);
            return string.Join(" ", words);
        }

        public string ReverseWord(string word)
        {
            if (string.IsNullOrWhiteSpace(word))
            {
                return word;
            }
            var item = string.Join("", word.Reverse());

            return item;
        }
    }
}