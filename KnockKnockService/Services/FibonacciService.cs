﻿using System;

namespace KnockKnockService.Services
{
    public class FibonacciService
    {
        public long GetNum(long index)
        {
            if (index == 0)
            {
                return 0;
            }

            if (index >= int.MaxValue || index <= int.MinValue)
            {
                throw new ArgumentOutOfRangeException();
            }

            if (index > 0)
            {
                if (index <= 2)
                {
                    return 1;
                }

                var k = (index) / 2;
                var a = GetNum(k + 1);
                var b = GetNum(k);

                if (index % 2 == 1)
                {
                    return (a * a + b * b);
                }
                else
                {
                    return b * (2 * a - b);
                }
            }
            else
            {
                if (index % 2 == 0)
                {
                    return -GetNum(-index);
                }
                else
                {
                    return GetNum(-index);
                }
            }
        }
    }
}