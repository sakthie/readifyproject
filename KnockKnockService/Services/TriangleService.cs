﻿using KnockKnockService.Models;

namespace KnockKnockService.Services
{
    public class TriangleService
    {
        public KnockKnockService.Models.TriangleType GetType(int a, int b, int c)
        {
            if (a <= 0 || b <= 0 || c <= 0)
            {
                return TriangleType.Error;
            }

            if (a + b <= c || a + c <= b || b + c <= a)
            {
                return TriangleType.Error;
            }

            if (a == b && b == c)
            {
                return TriangleType.Equilateral;
            }

            if (a == b || b == c || a == c)
            {
                return TriangleType.Isosceles;
            }

            return TriangleType.Scalene;
        }
    }
}